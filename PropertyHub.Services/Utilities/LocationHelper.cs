﻿using System;
using PropertyHub.Services.Common;
using PropertyHub.Services.Models;

namespace PropertyHub.Services.Utilities
{
    public static class LocationHelper
    {
        /// <summary> Calculates the new-point from a given source at a given range (meters) and bearing (degrees).</summary> 
        /// <param name="geoLocation">Orginal Location</param> 
        /// <returns>End-point from the source given the desired range and bearing.</returns> 
        public static Location CalculateCoordinates(Location geoLocation)
        {
            var latShifter = (double) geoLocation.Latitude * Constants.DegreesToRadians;
            var lonShifter = (double) geoLocation.Longitude * Constants.DegreesToRadians;
            var angularDistance = Constants.LocationOffset/Constants.EarthRadius;

            var offsetLat =
                Math.Asin(Math.Sin(latShifter) * Math.Cos(angularDistance) +
                          Math.Cos(latShifter) * Math.Sin(angularDistance))*Constants.RadiansToDegrees;
            var dlon =
                Math.Atan2(Math.Sin(angularDistance) * Math.Cos(latShifter),
                    Math.Cos(angularDistance) - Math.Sin(latShifter) * Math.Sin(offsetLat))*Constants.RadiansToDegrees;

            var offsetLong = ((lonShifter + dlon + Math.PI)%(Math.PI*2)) - Math.PI;

            return new Location {Latitude = (decimal) offsetLat, Longitude = (decimal) offsetLong};

        }

    }
}
