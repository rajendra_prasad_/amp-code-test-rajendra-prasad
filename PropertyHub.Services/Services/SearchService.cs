﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Services.Extensions;
using PropertyHub.Services.Interfaces;
using PropertyHub.Services.Models;
using PropertyHub.Services.Utilities;

namespace PropertyHub.Services.Services
{
    public class SearchService: ISearchService
    {
        public bool MatchByName(Property agencyProperty, Property dbProperty)
        {
            return agencyProperty.Name == dbProperty.Name;
        }

        public bool MatchByLocation(Property agencyProperty, Property dbProperty)
        {
            var geoLocation = new Location
            {
                Latitude = dbProperty.Latitude,
                Longitude = dbProperty.Longitude
            };

            var geoCoordinates = LocationHelper.CalculateCoordinates(geoLocation);

            return agencyProperty.Latitude <= geoCoordinates.Latitude &&
                   agencyProperty.Longitude <= geoCoordinates.Longitude;
        }

        public bool MatchByReverseName(Property agencyProperty, Property dbProperty)
        {
            return agencyProperty.Name.ReverseString() == dbProperty.Name;
        }

        public bool MatchByAllFields(Property agencyProperty, Property dbProperty)
        {
            return agencyProperty.Name == dbProperty.Name &&
                  agencyProperty.AgencyCode == dbProperty.AgencyCode &&
                  agencyProperty.Latitude == dbProperty.Latitude &&
                  agencyProperty.Longitude == dbProperty.Longitude &&
                  agencyProperty.Address == dbProperty.Address;
        }
    }
}
