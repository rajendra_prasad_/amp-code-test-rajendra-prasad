﻿using System;
using PropertyHub.Services.Common;
using PropertyHub.Services.Interfaces;
using PropertyHub.Services.Models;

namespace PropertyHub.Services.Services
{
    public class PropertyService: IPropertyMatcher
    {
        private readonly ISearchService _searchService;

        public PropertyService(ISearchService searchService)
        {
            _searchService = searchService;
        }

        public bool IsMatch(Property agencyProperty, Property databaseProperty)
        {
            var agency = (Constants.AgencyCode)Enum.Parse(typeof(Constants.AgencyCode), agencyProperty.AgencyCode);

            switch (agency)
            {
                case Constants.AgencyCode.OTBRE:
                    return _searchService.MatchByName(agencyProperty, databaseProperty);
                case Constants.AgencyCode.LRE:
                    return _searchService.MatchByLocation(agencyProperty, databaseProperty);
                case Constants.AgencyCode.CRE:
                    return _searchService.MatchByReverseName(agencyProperty, databaseProperty);
                default:
                    return _searchService.MatchByAllFields(agencyProperty, databaseProperty);
            }
        }
    }
}
