﻿using Autofac;
using PropertyHub.Services.Interfaces;
using PropertyHub.Services.Services;

namespace PropertyHub.Services.Config
{
    public class AutofacConfig: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
           builder.RegisterType<SearchService>().As<ISearchService>();
        }
    } 
}
