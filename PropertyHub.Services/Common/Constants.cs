﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Services.Common
{
    public static class Constants
    {
        #region CONSTANTS

        public enum AgencyCode
        {
            OTBRE,
            LRE,
            CRE
        }

        public static readonly double EarthRadius = 6378137.0;
        public static readonly double LocationOffset = 200;
        public static readonly double DegreesToRadians = 0.0174532925;
        public static readonly double RadiansToDegrees = 57.2957795;

        #endregion

        #region ERRORCodes

        #endregion

    }
}
