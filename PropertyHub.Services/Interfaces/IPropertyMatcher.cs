﻿using PropertyHub.Services.Models;

namespace PropertyHub.Services.Interfaces
{
    public interface IPropertyMatcher
    {
        bool IsMatch(Property agencyProperty, Property databaseProperty);
    }
}
