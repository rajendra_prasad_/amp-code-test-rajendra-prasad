﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Services.Models;

namespace PropertyHub.Services.Interfaces
{
    public interface ISearchService
    {
        bool MatchByName(Property agencyProperty, Property dbProperty);
        bool MatchByLocation(Property agencyProperty, Property dbProperty);
        bool MatchByReverseName(Property agencyProperty, Property dbProperty);
        bool MatchByAllFields(Property agencyProperty, Property dbProperty);
    }
}
