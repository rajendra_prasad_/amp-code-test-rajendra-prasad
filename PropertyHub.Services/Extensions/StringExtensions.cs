﻿using System.Linq;
using System.Security.Policy;
using System.Text.RegularExpressions;

namespace PropertyHub.Services.Extensions
{
    public static class StringExtensions
    {
        public static string ReverseString(this string name)
        {
            return string.Join(" ", name.Split(' ').Reverse());
        }

        public static string RemoveSpecialCharacters(this string input)
        {
            var regPattern = new Regex("",RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return regPattern.Replace(input, string.Empty);
        }
    }
}
