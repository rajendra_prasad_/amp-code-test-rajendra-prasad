﻿using System;
using NLog;

namespace PropertyHub.Services.Extensions
{
    public static class Utils
    {
        public static T SafeInvoke<T>(Func<T> func , T defaultValue, out Exception exception)
        {
            exception = null;

            try
            {
                return func();
            }
            catch (Exception ex)
            {
                exception = ex;

                try
                {
                    LogManager.GetLogger(typeof (Logger).ToString()).Error(exception);
                }
                catch (Exception)
                {    
                    //In case of any exceptions, do not throw error
                }
                return defaultValue;
            }
        }
    }
}
